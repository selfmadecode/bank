﻿using Bank.Services;
using Bank.Models;
using System;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            var option = new UserOperation();
            option.SelectOption();
            Console.ReadLine();
        }
    }
}
