﻿using Bank.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Services
{
    class BankTransactions : CreateUser, ITransactions // inherits creatuser and the interface for transaction
    {
        public static List<Customer> AllCustomers = new List<Customer>() { };
        public bool AccountExist;
        public double AccBalance = 100;
        Customer Customer = new Customer();
        

        public static void GetAllCustomer()
        {
            foreach (var user in AllCustomers)
            {
                Console.WriteLine($"{user.UserName}  {user.AccountNumber}");
            }
        }

        public void CheckAccountBalance()
        {
            foreach (var user in AllCustomers)
            {
                CheckUser();
                if (AccountExist)
                {
                    // AccBalance = Customer.Balance;
                    Console.WriteLine($"Available balance is now {user.Balance}");
                }
                else
                {
                    Console.WriteLine("Account does not Exist");
                }
            }
        }

        public void Deposit()
        {
            CheckUser();
            int amount;
            if (AccountExist)
            {
                Console.WriteLine("How much do you want to deposit");
                amount = Convert.ToInt32(Console.ReadLine());
                Customer.Balance += amount;
                Console.WriteLine($"Available balance is now {Customer.Balance}");
            }
            else
            {
                Console.WriteLine("Account does not Exist");
            }
        } // doesnt add to the user-balance
        public void TryDeposit()
        {
            try
            {
                foreach (var user in AllCustomers)
                {
                    CheckUser();
                    int amount;
                    if (AccountExist)
                    {
                        Console.WriteLine("How much do you want to deposit");
                        amount = Convert.ToInt32(Console.ReadLine());
                        user.Balance += amount;
                        Console.WriteLine($"Available balance is: {user.Balance}");
                    }
                    else
                    {
                        Console.WriteLine("Account does not Exist");
                    }
                }
            }
            catch (Exception error)
            {

                Console.WriteLine($"Incorrect details supplied: {error.Message} ");
            }
            
        }

        public void WithDraw()
        {
            // throw new NotImplementedException();
            foreach (var user in AllCustomers) // checks through the customer
            {
                CheckUser(); // checks if the user exist
                int amount;
                if (AccountExist)
                {
                    Console.WriteLine("How much do you want to withdraw");
                    amount = Convert.ToInt32(Console.ReadLine());
                    if (amount <= user.Balance)
                    {
                        user.Balance -= amount;
                        // AccBalance -= amount;
                        Console.WriteLine($"Available balance is now: {user.Balance}");
                    }
                    else
                    {
                        Console.WriteLine("Insufficeint fund!");
                    }
                }
                else
                {
                    Console.WriteLine("Account Does not exist");
                }
            }
        }

        public void CheckUser() // checks if the account exist in the list
        {
            try
            {
                Console.WriteLine("Account Number required");
                var AccountNumber = Convert.ToInt32(Console.ReadLine());

                foreach (var user in AllCustomers)
                {
                    // Console.WriteLine($"{user.UserName}  {user.AccountNumber}");
                    if (user.AccountNumber == AccountNumber) // Account Matched
                        // Console.WriteLine($"Matched, it is: {AccountNumber}");
                        AccountExist = true;
                    else
                        // Console.WriteLine("Failed to find");
                        AccountExist = false;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
