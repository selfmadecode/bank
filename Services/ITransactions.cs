﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Services
{
    interface ITransactions
    {
        void WithDraw();
        void Deposit();
        void CheckAccountBalance();
    }
}
