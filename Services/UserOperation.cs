﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Services
{
    class UserOperation : BankTransactions // in herits the bank transaction which inherited the create user class
    {
        public void SelectOption()
        {
            var options = new BankTransactions();

            Console.WriteLine("What Do you want to do");
            Console.WriteLine("1. Create Account ");
            Console.WriteLine("2. Withdraw ");
            Console.WriteLine("3. Check Balance ");
            Console.WriteLine("4. Deposit ");
            var result = Console.ReadLine();

            switch (result)
            {
                case "1":
                    options.CreateUserDetails(); // can be called because the transaction class inherits the createUser class
                    SwitchBack();
                    break;
                case "2":
                    options.WithDraw();
                    SwitchBack();
                    break;
                case "3":
                    options.CheckAccountBalance();
                    SwitchBack();
                    break;
                case "4":
                    options.TryDeposit();
                    SwitchBack();
                    break; 

                default:
                    Console.WriteLine("Wrong Option");
                    break;
            }
        }
        public void SwitchBack()
        {
            Console.Write("Do you want to perform another Operation Y/N: ");
            var decison = Console.ReadLine().ToUpper();

            if (decison == "Y")
            {
                SelectOption();
            }
            else if(decison == "N")
            {
                Console.WriteLine("Thank you for banking with us!");
            }
            else
            {
                Console.WriteLine("Try again later");
            }
        }
    }
}
